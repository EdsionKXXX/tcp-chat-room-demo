package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	//主动连接服务器
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	if err!=nil {
		fmt.Println("连接服务器出现问题:",err)
	}
	fmt.Println("连接服务器成功,showuser为查看当前在线用户，rename|name为改名")
	//调用完毕关闭连接
	defer conn.Close()
	//给服务器发送内容
	inbuf:=make([]byte,1024*2)//键盘输入缓存
	go func() {

		for  {

			n, err2 := os.Stdin.Read(inbuf)//接收键盘输入内容到缓存

			if err2!=nil {
				fmt.Println(err2)
				return
			}
			//给服务器发送内容
			conn.Write(inbuf[:n])
		}
	}()


	//接收服务器信息
	buf:=make([]byte,1024)
	for  {
		n, err1 := conn.Read(buf)//接收服务器请求
		if err1!=nil {
			fmt.Println(err1)
			return
		}
		fmt.Println(string(buf[:n-1]))

	}





}
