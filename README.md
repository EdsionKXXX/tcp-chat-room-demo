# tcp聊天室demo

#### 介绍
基于goland开发的高并发聊天室服务器，带测试客户端，可实现多用户聊天室同时上线广播，退出广播，聊天广播


#### 软件架构
如图
![流程图](https://images.gitee.com/uploads/images/2022/0509/202058_7cbf65fe_10803597.png "屏幕截图.png")


#### 安装教程

已经编译成二进制exe程序，将客户端复制多份即可运行

#### 使用说明

直接写内容发送聊天室广播
showuser为展示当前在线用户
rename|name为更改名字

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
